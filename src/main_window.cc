#include "main_window.h"
#include "ui_main_window.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
    ,ui_object(new Ui::main_window_ui)
{
    ui_object->setupUi(this);
    connect(ui_object->pbtn_close, &QPushButton::clicked, qApp, &QApplication::quit);
    SettingUpVolume();
}

MainWindow::~MainWindow() = default;


void MainWindow::SettingUpVolume()
{
    int volume_level = 0;
    std::system("amixer get Master | tail -n 1 | awk -F ' ' '{print $5}' | tr -d '[]%' > /tmp/cpwos_volume_file");


    std::string text_volume;
    std::ifstream requested_file("/tmp/cpwos_volume_file");
    std::getline(requested_file, text_volume);
    requested_file.close();

    if(text_volume.size() > 0)
        volume_level = std::atoi(text_volume.c_str());

    ui_object->hsli_volume->setValue(volume_level);

    std::cout << "Volume level: " << volume_level << std::endl;

    int volume_mic_level = 0;
    std::system("amixer get Capture | tail -n 1 | awk -F ' ' '{print $5}' | tr -d '[]%' > /tmp/cpwos_volume_mic_file");


    std::string text_volume_mic;
    std::ifstream tmp_volume_mic("/tmp/cpwos_volume_mic_file");
    std::getline(tmp_volume_mic, text_volume_mic);
    requested_file.close();

    if(text_volume.size() > 0)
        volume_mic_level = std::atoi(text_volume_mic.c_str());

    ui_object->hsli_mic->setValue(volume_mic_level);

    std::cout << "Volume Mic level: " << volume_mic_level << std::endl;
}

void MainWindow::ManageVolume()
{

}

void MainWindow::ManageVolumeMic()
{

}

void MainWindow::VolumeMute()
{

}

void MainWindow::VolumeMicMute()
{

}
