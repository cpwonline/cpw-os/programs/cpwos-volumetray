#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include <iostream>
#include <fstream>

#include <QMainWindow>
#include <QScopedPointer>


namespace Ui
{
	class main_window_ui;
}


class MainWindow : public QMainWindow
{
	Q_OBJECT

	public:
		explicit MainWindow(QWidget *parent = nullptr);
		~MainWindow() override;

        void SettingUpVolume();
        void ManageVolume();
        void ManageVolumeMic();
        void VolumeMute();
        void VolumeMicMute();

	private:
		QScopedPointer<Ui::main_window_ui> ui_object;
};

#endif // MAINWINDOW_H
